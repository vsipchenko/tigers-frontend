import { TigersFrontendPage } from './app.po';

describe('tigers-frontend App', function() {
  let page: TigersFrontendPage;

  beforeEach(() => {
    page = new TigersFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
