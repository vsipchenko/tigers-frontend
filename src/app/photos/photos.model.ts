export class Photo {
  album_name: string;
  photo_original: string;
  photo_cropped: string;
}
