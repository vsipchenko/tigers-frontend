import { Component, OnInit } from '@angular/core';
import {Photo} from "./photos.model";
import {PhotoService} from "./photos.service";


@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css'],
  providers: [PhotoService]
})


export class PhotosComponent implements OnInit {
  public photos : Photo[];

  constructor(private albumService: PhotoService) {
  }

  ngOnInit() {
    this.albumService.getPhotos().subscribe(photos => this.photos = photos);
  }

}
