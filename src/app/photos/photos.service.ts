import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/map';
import {CONFIG} from "../../main";
import {Observable} from "rxjs/Observable";
import {Photo} from "./photos.model";


@Injectable()
export class PhotoService {
  constructor(private http: Http) {
  }

  getPhotos(): Observable<Photo[]> {
    return this.http.get(CONFIG.api_root + 'albums/1/photos/')
      .map(response => response.json());
  }

}
