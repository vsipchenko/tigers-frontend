import { BrowserModule } from '@angular/platform-browser';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import {routing, appRoutingProviders} from "./app-routing.module";
import {CarouselDemoComponent} from "./home/carousel/carousel.component";
import {CarouselModule, AlertModule, ModalModule} from "ng2-bootstrap";
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';
import {AgmCoreModule} from "angular2-google-maps/core";
import {CommonModule} from "@angular/common";
import { NewsComponent } from './news/news.component';
import { PhotosComponent } from './photos/photos.component';
import { TrainingComponent } from './home/training/training.component';
import { AlbumsComponent } from './albums/albums.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CarouselDemoComponent,
    AboutComponent,
    ContactsComponent,
    NewsComponent,
    PhotosComponent,
    TrainingComponent,
    AlbumsComponent,
  ],
  imports: [
    AlertModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    CarouselModule,
    ModalModule,
    CommonModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAUNq0auZeRdyw2xf7FSLZEzUk7doC0ihs'
    })
  ],
  // schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }


