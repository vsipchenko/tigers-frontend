export class Contacts {
  first_phone: string;
  second_phone: string;
  email: string;
  vk_link: string;
  fb_link: string;
  other: string
}
