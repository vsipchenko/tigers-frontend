import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/map';
import {CONFIG} from "../main";
import {Observable} from "rxjs/Observable";
import {Contacts} from "./app.contacts.model";


@Injectable()
export class ContactsService {
  contacts: Contacts;
  constructor(private http: Http) {
  }

  getContacts(): Observable<Contacts> {
    return this.http.get(CONFIG.api_root + 'contacts/')
      .map(response =>response.json());
  }

}
