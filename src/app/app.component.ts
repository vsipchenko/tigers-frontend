import {Component, OnInit} from '@angular/core';
import {Contacts} from "./app.contacts.model";
import {ContactsService} from "./app.contacts.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ContactsService]
})
export class AppComponent implements OnInit {
  contacts: Contacts;
  page: string = 'home';


  constructor(private contactsService: ContactsService) {
  };

  ngOnInit() {
    this.contactsService.getContacts().subscribe(contacts => this.contacts = contacts)
      }


}
