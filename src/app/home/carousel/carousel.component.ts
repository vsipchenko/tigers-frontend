import {Component, OnInit} from '@angular/core';
import {HomeService} from "../home.service";
import {HomePageSliders} from "../home.model";


@Component({
  selector: 'home-carousel',
  styleUrls: ['./carousel.component.css'],
  templateUrl: './carousel.component.html',
  providers: [HomeService]
})

export class CarouselDemoComponent implements OnInit {
  public myInterval: number = 5000;
  public noWrapSlides: boolean = false;
  public slides: HomePageSliders[];

  public constructor(private homeService: HomeService) {
  }

  ngOnInit() {
    this.homeService.getHomePageSliders().subscribe(homePageSliders => this.slides = homePageSliders)
  }

}
