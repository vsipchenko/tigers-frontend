import {Injectable, OnInit} from '@angular/core';
import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';
import {CONFIG} from "../../main";
import {Home, IHome, HomePageSliders, Training} from "./home.model";
import {Observable} from "rxjs/Observable";


@Injectable()
export class HomeService {
  constructor(private http: Http) {
  }

  getData(): Observable<Home> {
    return this.http.get(CONFIG.api_root + 'home/')
      .map(response => response.json());
  }

  getHomePageSliders(): Observable<HomePageSliders[]> {
    return this.http.get(CONFIG.api_root + 'sliders/')
      .map(response => response.json());
  }

  getTraining(): Observable<Training[]> {
    return this.http.get(CONFIG.api_root + 'training/')
      .map(response => response.json());
  }

}
