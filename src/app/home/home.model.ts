export interface IHome {
  about_ultimate_short_note: string;
  team_introduction_text: string;
  team_introduction_title: string;
  welcome_text: string;
  welcome_title: string;
}

export class Home {
  about_ultimate_short_note: string;
  team_introduction_text: string;
  team_introduction_title: string;
  welcome_text: string;
  welcome_title: string;
}

export class HomePageSliders {
  cropped_image_url: string
}

export class Training{
  day: string;
  time: string;
  location: string;
  price: string;
  latitude_coordinates: number;
  length_coordinates: number
}
