import {Component, OnInit, ViewChild} from '@angular/core';

import {HomeService} from "./home.service";
import {Home, IHome, HomePageSliders} from "./home.model";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [HomeService]
})

export class HomeComponent implements OnInit {
  homeData: Home;


  constructor(private homeService: HomeService) {
  };

  ngOnInit() {
    this.homeService.getData().subscribe(homeData => this.homeData = homeData);

  }

}

// webpack html imports
