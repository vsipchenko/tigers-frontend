import {Component, OnInit, ViewChild} from '@angular/core';
import {Training} from "../home.model";
import {HomeService} from "../home.service";
import {ModalDirective} from "ng2-bootstrap";

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {
  public training: Training[];
  zoom: number = 15;
  public constructor(private homeService: HomeService) {
  }

  ngOnInit() {
    this.homeService.getTraining().subscribe(training => this.training = training)
  }


  @ViewChild('childModal') public childModal: ModalDirective;

  public showChildModal(): void {
    this.childModal.show();
  }

  public hideChildModal(): void {
    this.childModal.hide();
  }

}
