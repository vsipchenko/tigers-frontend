import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/map';
import {CONFIG} from "../../main";
import {Observable} from "rxjs/Observable";
import {Album} from "./album.model";


@Injectable()
export class AlbumService {
  constructor(private http: Http) {
  }

  getAlbums(): Observable<Album[]> {
    return this.http.get(CONFIG.api_root + 'albums/')
      .map(response => response.json());
  }

}
