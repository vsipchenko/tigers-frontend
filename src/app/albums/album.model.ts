export class Album {
  id: number;
  name: string;
  photos_count: number;
  cover: string;
}
