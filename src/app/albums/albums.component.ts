import { Component, OnInit } from '@angular/core';
import {Album} from "./album.model";
import {AlbumService} from "./album.service";


@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css'],
  providers: [AlbumService]
})
export class AlbumsComponent implements OnInit {
  public albums : Album[];

  constructor(private albumService: AlbumService) {
  }

  ngOnInit() {
    this.albumService.getAlbums().subscribe(albums => this.albums = albums);
  }

}
