import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/map';
import {CONFIG} from "../../main";
import {Observable} from "rxjs/Observable";
import {News} from "./news.model";


@Injectable()
export class NewsService {
  constructor(private http: Http) {
  }

  getNews(): Observable<News[]> {
    return this.http.get(CONFIG.api_root + 'news/')
      .map(response => response.json());
  }
}
