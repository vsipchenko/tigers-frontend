export class News {
  title: string;
  text: string;
  cropped_image_url: string;
  pretty_date: string;
}
