import {Component, OnInit} from '@angular/core';
import {NewsService} from "./news.service";
import {News} from "./news.model";

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
  providers: [NewsService]
})
export class NewsComponent implements OnInit {
  public news : News[];

  public constructor(private newsService: NewsService) {
  };

  ngOnInit() {
    this.newsService.getNews().subscribe(news => this.news = news);

  }

}
