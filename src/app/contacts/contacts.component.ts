import { Component, OnInit } from '@angular/core';
import {ContactsService} from "../app.contacts.service";
import {Contacts} from "../app.contacts.model";

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css'],
  providers: [ContactsService]
})
export class ContactsComponent implements OnInit {


  constructor() {
  };

  ngOnInit() {
  }
}

